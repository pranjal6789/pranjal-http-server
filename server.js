const http = require("http");
const fs = require("fs");
const { v4: uuid4 } = require("uuid");

function onRequest(request, response) {
  if (
    request.url === "/html" ||
    (request.url.split("/").length === 3 &&
      request.url.split("/")[2] === "" &&
      request.url === "/html/")
  ) {
    fs.readFile("./index.html", "utf-8", (err, data) => {
      if (err) {
        response.writeHead(404);
        response.write("File not found");
        response.end();
      } else {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(data);
        response.end();
      }
  
    });
  } else if (
    request.url === "/json" ||
    (request.url.split("/").length === 3 &&
      request.url.split("/")[2] === "" &&
      request.url === "/json/")
  ) {
    fs.readFile("./input.json", "utf-8", (err, data) => {
      if (err) {
        response.writeHead(404);
        response.write("File  not found");
        response.end();
      } else {
        response.writeHead(200, { "Content-Type": "application/JSON" });
        response.write(data);
        response.end();
      }
    });
  } else if (
    request.url === "/uuid" ||
    (request.url.split("/").length === 3 &&
      request.url.split("/")[2] === "" &&
      request.url === "/uuid/")
  ) {
    response.writeHead(200, { "Content-Type": "application/JSON" });
    let getId = uuid4();
    let result = { UUID: getId };
    let finalResult = JSON.stringify(result);
    response.write(finalResult);
    response.end();
  } else if (
    request.url.split("/")[1] === "status" &&
    (request.url.split("/").length === 3 ||
      (request.url.split("/").length === 4 && request.url.split("/")[3] === ""))
  ) {
    if (request.url.split("/")[2] in http.STATUS_CODES) {
      response.writeHead(request.url.split("/")[2]);
      response.write(http.STATUS_CODES[request.url.split("/")[2]]);
      response.end();
    } else {
      console.log(request.url.split("/").length);
      response.writeHead(422);
      response.write("Wrong status code");
      response.end();
    }
  } else if (
    request.url.split("/")[1] === "delay" &&
    ((request.url.split("/").length === 3 &&
      request.url.split("/")[2] !== "" &&
      request.url.split("/")[2] > -1) ||
      (request.url.split("/").length === 4 &&
        request.url.split("/")[3] === "" &&
        request.url.split("/")[2] > -1))
  ) {
    if (!isNaN(request.url.split("/")[2])) {
      setTimeout(() => {
        response.write("Working");
        response.end();
      }, request.url.split("/")[2] * 1000);
    } else {
      response.writeHead(404);
      response.write("Url Incorrect");
      response.end();
    }
  } else {
    response.writeHead(404, { "Content-Type": "text" });
    response.write("URL not found");
    response.end();
  }
}
http.createServer(onRequest).listen(8000, () => {
  console.log("Server running");
});
